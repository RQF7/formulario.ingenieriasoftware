<%-- consultar.jsp
  -- Este archivo es parte de la aplicación web «Formulario».
  -- Fecha de cración: lunes 18 de septiembre de 2017 --%>

<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%
	ArrayList<ArrayList<String>> consulta = null;
	ArrayList<String> fallos = new ArrayList<String>();
	boolean despues_de_consulta =
			(request.getAttribute("despues_de_consulta") != null) ? true : false;
	boolean sin_registros = (request.getAttribute("sin_registros") != null)
			? true : false;
	boolean notificacion_fallo = false;
	boolean bandera_titulo = true;
	if (despues_de_consulta)
		consulta = (ArrayList<ArrayList<String>>) 
				request.getAttribute("consulta");
%>

<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>Consulta - Formulario</title>
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" 
				rel="stylesheet">
		<link type="text/css" rel="stylesheet" href="../css/materialize.css"  
				media="screen,projection"/>
		<style>
			#tit1 {
				font-weight: 600;
			}
			.input-field label {
				color: #333;
			}
			#formulario, #botones, #resultados {
				margin-top: 40px;
			}
			#botones button, #botones a {
				margin-top: 10px;
			}
		</style>
	</head>

	<body class="blue-grey lighten-5">

		<div class="container">
			<h3 class="center-align teal-text text-darker-2" id="tit1">
				Consultar
			</h3>
			<div class="divider grey"></div>

			<div class="row" id="formulario">
				<form class="col s12" method="post" action="hacerConsulta">
					<div class="input-field col s12">
						<input id="nombre" type="text" class="validate"
								name="curp" 
						<% if (despues_de_consulta) { %>
							value="<%= request.getAttribute("parametro") %>"
						<% } %>		
						>
						<label for="nombre">
							CURP (para múltiples consultas, separar con una coma)
						</label>
					</div>
					<div id="botones" class="col s12 center-align">
						<a class="teal darker-2 waves-effect waves-light btn"
								href="menu">
							<i class="material-icons left">arrow_back</i> 
							Regresar
						</a>	
						<button class="teal darker-2 btn waves-effect waves-light" 
								type="submit" name="action">
							Consultar
							<i class="material-icons right">search</i>
						</button>
					</div>
				</form>
			</div>

			<% if (despues_de_consulta) { %>
				<div id="resultados" class="row s12">
				<% for (ArrayList<String> registro : consulta) {
					if (registro.size() == 1) {
						notificacion_fallo = true;
						fallos.add(registro.get(0));
					} else { %>

						<% if (bandera_titulo) { 
							bandera_titulo = false; %>
							<h4 class="teal-text text-darker-2 center-align">
								Resultados:
							</h4> <br>
						<% } %>
			
						<div class="col s12 m6">			
							<h5 class="teal-text text-darker-2"> 
								<%= registro.get(0) %>
							</h5>
							<ul>
								<li>
									<b>Nombre:</b> <%= registro.get(1) %>
								</li>
								<li>
									<b>Apellido paterno:</b> 
										<%= registro.get(2) %>
								</li>
								<li>
									<b>Apellido materno:</b> 
										<%= registro.get(3) %>
								</li>
								<li>
									<b>Fecha de nacimiento:</b> 
										<%= registro.get(4) %>
								</li>
								<li>
									<b>Sexo:</b> <%= registro.get(7) %>
								</li>
								<li>
									<b>Dirección:</b> <%= registro.get(5) %>
								</li>
								<li>
									<b>Correo electrónico:</b> 
										<%= registro.get(6) %>
								</li>
							</ul>
						</div>

					<% } %> <%-- if/else --%>
				<% } %> <%-- for --%>
				</div> <%-- div de resultados --%>
			<% } %> <%-- if --%>

		</div>

		<% if (sin_registros) { %>
			<div id="MSG_sin_registros" class="modal">
				<div class="modal-content">
					<h4>Error</h4>
					<p>No se encontró ningún registro en el sistema</p>
				</div>
				<div class="modal-footer">
					<a href="menu" class="modal-action modal-close waves-effect 
						waves-green btn-flat">Aceptar</a>
				</div>
			</div>
		<% } %>

		<% if (notificacion_fallo) { %>
			<div id="MSG_fallos" class="modal">
				<div class="modal-content">
					<h4>Error</h4>
					<p>No se pudieron encontrar los siguientes registros:</p>
					<ul>
						<% for (String fallo : fallos) { %>
							<li><%= fallo %></li>
						<% } %>
					</ul>
				</div>
				<div class="modal-footer">
					<a href="#!" class="modal-action modal-close waves-effect 
						waves-green btn-flat">Aceptar</a>
				</div>
			</div>
		<% } %>

		<!-- Scripts -->
		<script type="text/javascript" src="../js/jquery.js"></script>
		<script type="text/javascript" src="../js/materialize.js"></script>
		<script type="text/javascript">
			
			/* Activar modal */
			$(document).ready(function() {
				$('.modal').modal();
			});

			<% if (sin_registros) { %>
				$(document).ready(function() {
					$('#MSG_sin_registros').modal('open');
				});
			<% } %>

			<% if (notificacion_fallo) { %>
				$(document).ready(function () {
					$('#MSG_fallos').modal('open');
				});
			<% } %>

		</script>
	</body>

</html>

