/* formulario.js
 * Este archivo es parte de la aplicación web «Formulario».
 * Fecha de cración: lunes 28 de septiembre de 2017 */

/* Activar selector de fecha */
$('.datepicker').pickadate({
	selectMonths: true,
	selectYears: 15,
	today: 'Today',
	clear: 'Clear',
	close: 'Ok',
	closeOnSelect: false
});

/* Activar select y modals */
$(document).ready(function() {
	$('select').material_select();
	$('.modal').modal();
});

/* Mensaje personalizado para campos incompletos */
document.addEventListener("DOMContentLoaded", function() {
	var inputs = document.getElementsByTagName("INPUT");
	for (var i = 0; i < inputs.length; i++) {
		inputs[i].oninvalid = function(e) {
			e.target.setCustomValidity("");
			if (!e.target.validity.valid) {
				/* MSJ3: Registro incompleto */
				e.target.setCustomValidity("Error, este campo " 
						+ "no se puede dejar en blanco");
			}
		};
		inputs[i].oninput = function(e) {
			e.target.setCustomValidity("");
		};
	}
})

/* Validaciones de longitud; en caso de error se muestra MSJ4 
 * La validación se hace de forma secuencial.
 *
 * TODO: Cambiar modals por indicadores en tiempo real; posiblemente más
 * usable. */

function validarLongitudes ()
{
	var nombre = document.getElementById("nombre").value;
	if (nombre.length < 3 || nombre.length > 25) {
		var mensaje = "El nombre debe tener entre 3 y 25 caracteres";
		mostrarError4(mensaje);
		return false;
	}

	var apPaterno = document.getElementById("apPaterno").value;
	if (apPaterno.length < 3 || apPaterno.length > 25) {
		var mensaje = "Los apellidos deben tener entre 3 y 25 caracteres";
		mostrarError4(mensaje);
		return false;
	}

	var apMaterno = document.getElementById("apMaterno").value;
	if (apMaterno.length < 3 || apMaterno.length > 25) {
		var mensaje = "Los apellidos deben tener entre 3 y 25 caracteres";
		mostrarError4(mensaje);
		return false;
	}
	
	var curp = document.getElementById("curp").value;
	if (curp.length != 18) {
		var mensaje = "El CURP debe tener 18 caracteres";
		mostrarError4(mensaje);
		return false;
	}
	
	var direccion = document.getElementById("direccion").value;
	if (direccion.length < 10 || direccion.length > 50) {
		var mensaje = "La dirección debe tener entre 10 y 50 caracteres";
		mostrarError4(mensaje);
		return false;
	}
	
	var correo = document.getElementById("correo").value;
	if (correo.length < 10 || correo.length > 50) {
		var mensaje = "El correo debe tener entre 10 y 50 caracteres";
		mostrarError4(mensaje);
		return false;
	}

	return true;
}

/* Muestra el modal con el mensaje 4.
 * Perdón por la mezcla de javascrip puro con los selectores de
 * jquery, pero materialize solo provee documentación para jquery. */

function mostrarError4 (mensaje)
{
	document.getElementById("contenido4").innerHTML = mensaje;
	$('#MSG4').modal('open');
}
