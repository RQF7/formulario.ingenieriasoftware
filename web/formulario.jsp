<%-- formulario.jsp
  -- Este archivo es parte de la aplicación web «Formulario».
  -- Fecha de cración: lunes 18 de septiembre de 2017 --%>

<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%
	ArrayList<String> catalogosexo
			= (ArrayList<String>) request.getAttribute("catalogosexo");
	boolean error_curp = (request.getAttribute("error_curp") != null)
			? true : false;
	boolean error_email = (request.getAttribute("error_email") != null)
			? true : false;
	boolean curp_duplicado = (request.getAttribute("curp_duplicado") != null)
			? true : false;
	boolean registro_exitoso = (request.getAttribute("registro_exitoso") != null)
			? true : false;
	boolean errores = error_curp || error_email || curp_duplicado;
%>

<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>Registro - Formulario</title>
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" 
				rel="stylesheet">
		<link type="text/css" rel="stylesheet" href="../css/materialize.css"  
				media="screen,projection"/>
		<style>
			#tit1 {
				font-weight: 600;
			}
			.input-field label {
				color: #333;
			}
			#formulario, #botones {
				margin-top: 40px;
			}
			#botones button, #botones a {
				margin-top: 10px;
			}
		</style>
	</head>

	<body class="blue-grey lighten-5">

		<div class="container">
			<h3 class="center-align teal-text text-darker-2" id="tit1">
				Registrar
			</h3>
			<div class="divider grey"></div>

			<div class="row" id="formulario">
				<form class="col s12" method="post" action="hacerRegistro"
						onsubmit="return validarLongitudes();">
					<div class="col s12 m6">
						<div class="input-field col s12">
							<input id="nombre" type="text" class="validate"
									name="nombre" required
							   <% if (errores) { %>
							   		value="<%=
										request.getAttribute("nombre") %>"
							   <% } %>
							   >
							<label for="nombre">Nombre</label>
						</div>
						<div class="input-field col s12">
							<input id="apPaterno" type="text" class="validate"
									name="apPaterno" required
							   <% if (errores) { %>
							   		value="<%=
										request.getAttribute("apPaterno") %>"
							   <% } %>
							   >
							<label for="apPaterno">Apellido Paterno</label>
						</div>
						<div class="input-field col s12">
							<input id="apMaterno" type="text" class="validate"
									name="apMaterno" required
							   <% if (errores) { %>
							   		value="<%=
										request.getAttribute("apMaterno") %>"
							   <% } %>
							   >
							<label for="apMaterno">Apellido Materno</label>
						</div>
						<div class="input-field col s12">
							<input id="curp" type="text" class="validate"
									name="curp" required
							   <% if (errores) { %>
							   		value="<%=
										request.getAttribute("curp") %>"
							   <% } %>
							   >
							<label for="curp">CURP</label>
						</div>
					</div>
					<div class="col s12 m6">
						<div class="input-field col s12">
							<input id="fecha" type="text" class="datepicker"
									name="fecha" required
							   <% if (errores) { %>
							   		value="<%=
										request.getAttribute("fecha") %>"
							   <% } %>
							   >
							<label for="fecha">Fecha de nacimiento</label>
						</div>
						
						<!-- TODO: Alineación de selección extraña en
						  -- chromium. -->

						<div class="input-field col s12">
							<select name="sexo" required>
								<option value="" disabled selected>Género</option>
							<% for (int i = 0; i < catalogosexo.size(); i++) {%>
								<option value="<%= catalogosexo.get(i) %>"
								<%
									if (errores)
										if (request.getAttribute("sexo")
												.equals(catalogosexo.get(i)))
											out.print("selected");
								%> 
								>
								<%= catalogosexo.get(i)%>
								</option>
							<% } %>
							</select>
						</div>	
						<div class="input-field col s12">
							<input id="direccion" type="text" class="validate"
									name="direccion" required
							   <% if (errores) { %>
							   		value="<%=
										request.getAttribute("direccion") %>"
							   <% } %>
							   >
							<label for="direccion">Dirección</label>
						</div>
						<div class="input-field col s12">
							<input id="correo" type="text" class="validate"
									name="correo" required
							   <% if (errores) { %>
							   		value="<%=
										request.getAttribute("correo") %>"
							   <% } %>
							   >
							<label for="correo">Correo electrónico</label>
						</div>
					</div>
					<div id="botones" class="col s12 right-align">
						<a class="teal darker-2 waves-effect waves-light btn"
								href="menu">
							<i class="material-icons left">arrow_back</i> 
							Regresar
						</a>	
						<button class="teal darker-2 btn waves-effect waves-light" 
								type="submit">
							Registrar
							<i class="material-icons right">add_circle</i>
						</button>
					</div>
				</form>
			</div>
		</div>

		<div id="MSG4" class="modal">
			<div class="modal-content">
				<h4>Error</h4>
				<p id="contenido4">Se excedió la longitud máxima del campo</p>
			</div>
			<div class="modal-footer">
				<a href="#!" class="modal-action modal-close waves-effect 
					waves-green btn-flat">Aceptar</a>
			</div>
		</div>

		<% if (error_curp) { %>
			<div id="MSG_curp" class="modal">
				<div class="modal-content">
					<h4>Error</h4>
					<p>El formato del CURP es incorrecto.</p>
				</div>
				<div class="modal-footer">
					<a href="#!" class="modal-action modal-close waves-effect 
						waves-green btn-flat">Aceptar</a>
				</div>
			</div>
		<% } %>

		<% if (error_email) { %>
			<div id="MSG_email" class="modal">
				<div class="modal-content">
					<h4>Error</h4>
					<p>El formato del correo es incorrecto.</p>
				</div>
				<div class="modal-footer">
					<a href="#!" class="modal-action modal-close waves-effect 
						waves-green btn-flat">Aceptar</a>
				</div>
			</div>
		<% } %>

		<% if (curp_duplicado) { %>
			<div id="MSG_duplicado" class="modal">
				<div class="modal-content">
					<h4>Error</h4>
					<p>Ya se registró una persona con el mismo CURP.</p>
				</div>
				<div class="modal-footer">
					<a href="#!" class="modal-action modal-close waves-effect 
						waves-green btn-flat">Aceptar</a>
				</div>
			</div>
		<% } %>

		<% if (registro_exitoso) { %>
			<div id="MSG_exito" class="modal">
				<div class="modal-content">
					<h4>Éxito</h4>
					<p>El registro se realizó exitosamente.</p>
				</div>
				<div class="modal-footer">
					<a href="#!" class="modal-action modal-close waves-effect 
						waves-green btn-flat">Aceptar</a>
				</div>
			</div>
		<% } %>

		<!-- Scripts -->
		<script type="text/javascript" src="../js/jquery.js"></script>
		<script type="text/javascript" src="../js/materialize.js"></script>
		<script type="text/javascript" src="../js/formulario.js"></script>
		<script type="text/javascript">
			<% if (error_curp) { %>
				$(document).ready(function() {
					$('#MSG_curp').modal('open');
				});
			<% } %>
			<% if (error_email) { %>
				$(document).ready(function() {
					$('#MSG_email').modal('open');
				});
			<% } %>
			<% if (curp_duplicado) { %>
				$(document).ready(function() {
					$('#MSG_duplicado').modal('open');
				});
			<% } %>
			<% if (registro_exitoso) { %>
				$(document).ready(function() {
					$('#MSG_exito').modal('open');
				});
			<% } %>
		</script>
	</body>

</html>
