<%-- inicio.jsp
  -- Este archivo es parte de la aplicación web «Formulario».
  -- Fecha de cración: lunes 18 de septiembre de 2017 --%>

<%@ page contentType="text/html; charset=UTF-8" %>

<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>Menú - Formulario</title>
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" 
				rel="stylesheet">
		<link type="text/css" rel="stylesheet" href="../css/materialize.css"  
				media="screen,projection"/>
		<style>
			body {
				background-color: #080f17;
			}
			#tit1, #tit2{
				font-weight: 600;
			}
			#botones {
				margin-top: 80px;
			}
			#botones>div:first-child {
				margin-bottom: 40px;
			}
		</style>
	</head>

	<body>

		<div class="container">
			<h3 class="center-align grey-text text-lighten-5" id="tit1">
				Equipo 11
			</h3>
			<div class="divider"></div>
			<h5 class="center-align grey-text text-lighten-5" id="tit2">
				José Carlos Castillo Amador | Rocío Vivia Delgadillo
			</h5>

			<div id="botones">
				<div class="row center-align">
					<a class="teal darker-2 waves-effect waves-light btn-large"
							href="registrar">
						<i class="material-icons right">add_circle</i> 
						REGISTRAR
					</a>	
				</div>
				<div class="row center-align">
					<a class="teal darker-2 waves-effect waves-light btn-large"
							href="consultar">
						<i class="material-icons right">search</i> 
						CONSULTAR
					</a>	
				</div>
			</div>
		</div>

		<!-- Scripts -->
		<script type="text/javascript" src="../js/jquery.js"></script>
		<script type="text/javascript" src="../js/materialize.js"></script>
	</body>

</html>
