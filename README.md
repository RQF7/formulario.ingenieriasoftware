# Ejercicio de formulario #

## Pendientes ##

* Correcciones a documento de diseño.

## Requisitos ##

Versiones en las que se desarrolló; posiblemente corra en algunas otras:

* Java JDK8
* Tomcat8
* MySQL o MariaDB

## Capturas ##

Solo con la finalidad de que se verifique si todos lo vemos igual.



## Equipos ##

### 1. Análisis: equipo 11 ###
* Rocío Vivia Delgadillo
* José Carlos Castillo Amador

### 2. Diseño: equipo 3 ###
* Alejandro Esquivel
* ??

### 3. Programación: equipo 5 ###
* Ricardo Quezada Figueroa
* Salomón García Olmedo

### 4. Calidad: equipo 6 ###
* Daniel Ayala Zamorano
* Alondra Mejía

### 5. Manuales: equipo 13 ###
* Javier Sánchez Gama
* Karla Victoria León 
