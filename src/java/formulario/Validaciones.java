/* Validaciones.java
 * Este archivo es parte de la aplicación web «Formulario».
 * Fecha de cración: lunes 18 de septiembre de 2017 */

package formulario;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * Clase que agrupa métodos para validar campos del formulario.
 * @author Ricardo Quezada
 */

public class Validaciones {

	private Pattern expCURP;
	private Pattern expEmail;

	public Validaciones ()
	{
		expCURP = Pattern.compile(
			"^[A-Z]{1}[AEIOU]{1}[A-Z]{2}" +
			"[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])" +
			"[HM]{1}" +
			"(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|" +
			"MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)" +
			"[B-DF-HJ-NP-TV-Z]{3}" +
			"[0-9A-Z]{1}" +
			"[0-9]{1}$"
		);
		expEmail = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
	}

	public boolean validarCURP (String curp)
	{
		Matcher evaluacion = expCURP.matcher(curp);
		return evaluacion.matches();
	}

	public Boolean validarCorreo (String correo){

    	Matcher evaluacion = expEmail.matcher(correo);
        return evaluacion.matches();
    }

}
