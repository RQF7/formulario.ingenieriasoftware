/* Formulario.java
 * Este archivo es parte de la aplicación web «Formulario».
 * Fecha de cración: lunes 18 de septiembre de 2017 */

package formulario;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet principal de aplicación web.
 * Ruta de servlet: <i>/formulario</i>.
 * Rutas de acciones disponibles:
 * <ul>
 * 		<li>
 *			<i>/menu</i>: Página principal de sitio. Contenido en menu.jsp.
 *		</li>
 * 		<li>
 *			<i>/registrar</i>:  Página con formulario de registro.
 *			Contenido en formulario.jsp
 *		</li>
 * 		<li>
 *			<i>/hacerRegistro</i>:  Valida e inserta un nuevo registro.
 *		</li>
 * </ul>
 * @author Ricardo Quezada
 */

@WebServlet(name = "Formulario", urlPatterns = {"/Formulario"})
public class Formulario extends HttpServlet {

	public String nombre,
				  apPaterno,
				  apMaterno,
				  CURP,
				  sexo,
				  direccion,
				  correo,
				  fecha2;
	public Date fecha;
	private ConexionBD conexionBD;
	private Validaciones validaciones;
	private Map<String, Integer> diccionarioMeses;

	/**
	 * Constructor de servlet.
	 * Solo es llamado por el contenedor de servlet (tomcat o glassfish).
	 * Inicia una conexión con la base de datos y crea una instancia de
	 * «Validaciones» para uso posterior.
	 */

	public Formulario ()
	{
		try {
			conexionBD = new ConexionBD();
			validaciones = new Validaciones();
		} catch (Exception e) {
			e.printStackTrace();
		}

		diccionarioMeses = new HashMap<String, Integer>();
		diccionarioMeses.put("January,", 0);
		diccionarioMeses.put("February,", 1);
		diccionarioMeses.put("March,", 2);
		diccionarioMeses.put("April,", 3);
		diccionarioMeses.put("May,", 4);
		diccionarioMeses.put("June,", 5);
		diccionarioMeses.put("July,", 6);
		diccionarioMeses.put("August,", 7);
		diccionarioMeses.put("September,", 8);
		diccionarioMeses.put("October,", 9);
		diccionarioMeses.put("November,", 11);
		diccionarioMeses.put("December,", 11);
	}

	/**
	 * Recepción de cualquier tipo de petición.
	 * Filtra la petición de acuerdo a lo establecido en la descripción de la
	 * clase. En caso de que la petición no responda a ningún formato conocido
	 * responde con 404.
	 */

	@Override
	public void service (HttpServletRequest peticion,
	HttpServletResponse respuesta)
	throws IOException, ServletException
	{
		String informacion = peticion.getPathInfo();
		if (informacion.equals("/menu")) {

			RequestDispatcher vista =
				peticion.getRequestDispatcher("/inicio.jsp");
			vista.forward(peticion, respuesta);

		} else if (informacion.equals("/registrar")) {

			construirFormulario(peticion, respuesta);

		} else if (informacion.equals("/hacerRegistro")) {

			guardarDatos(peticion, respuesta);

		} else if (informacion.equals("/consultar")) {

			construirPaginaConsulta(peticion, respuesta);

		} else if (informacion.equals("/hacerConsulta")) {

			consultarDatos(peticion, respuesta);

		} else if (informacion.equals("/")) {
		
			respuesta.sendRedirect("menu");

		} else {

			respuesta.sendError(HttpServletResponse.SC_NOT_FOUND,
				"Información de path: " + informacion);
		
		}
	}

	public void construirFormulario (HttpServletRequest peticion,
	HttpServletResponse respuesta)
	throws IOException, ServletException
	{
		/* Obtener el catálogo de la base */
		ArrayList<String> catalogoSexo = null;
		try {
			catalogoSexo = conexionBD.obtenerCatalogoSexo();
		} catch (Exception e) {
			e.printStackTrace();
			respuesta.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE,
					"Excepción SQL: " + e);
			return;
		}

		/* Obtener registro de mensajes de la sesión */
		HttpSession sesion = peticion.getSession(true);
		if (sesion.getAttribute("error_curp") != null) {

			peticion.setAttribute("error_curp", "");
			sesionAAtributos(sesion, peticion);
			sesion.invalidate();

		} else if (sesion.getAttribute("error_email") != null) {

			peticion.setAttribute("error_email", "");
			sesionAAtributos(sesion, peticion);
			sesion.invalidate();

		} else if (sesion.getAttribute("curp_duplicado") != null) {

			peticion.setAttribute("curp_duplicado", "");
			sesionAAtributos(sesion, peticion);
			sesion.invalidate();

		} else if (sesion.getAttribute("registro_exitoso") != null) {
		
			peticion.setAttribute("registro_exitoso", "");
			sesion.invalidate();

		}

		/* Redirigir a JSP */
		RequestDispatcher vista =
			peticion.getRequestDispatcher("/formulario.jsp");
		peticion.setAttribute("catalogosexo", catalogoSexo);
		vista.forward(peticion, respuesta);	
	}

	/**
	 * Valida la entrada del usuario e introduce un nuevo registro en la
	 * base de datos. En caso de algún error con la entrada, se regresa a
	 * formulario.jsp con un mensaje de error.
	 */

	public void guardarDatos (HttpServletRequest peticion,
	HttpServletResponse respuesta)
	throws IOException, ServletException
	{
		/* Obtener argumentos */
		nombre = peticion.getParameter("nombre");
		apPaterno = peticion.getParameter("apPaterno");
		apMaterno = peticion.getParameter("apMaterno");
		CURP = peticion.getParameter("curp");
		sexo = peticion.getParameter("sexo");
		fecha2 = peticion.getParameter("fecha");
		direccion = peticion.getParameter("direccion");
		correo = peticion.getParameter("correo");

		/* Validación de campos en POST */
		if (nombre == null || apPaterno == null || apMaterno == null
				|| CURP == null || sexo == null || fecha2 == null 
				|| direccion == null || correo == null) {

			/* Petición equivocada */		
			respuesta.sendRedirect("menu");
			return;
		}

		/* Validación de campos requeridos */
		if (nombre.equals("") || apPaterno.equals("") || apMaterno.equals("")
				|| CURP.equals("") || fecha2.equals("") 
				|| direccion.equals("") || correo.equals("")) {

			/* Petición válida pero maliciosa:
			 * El usuario intentó saltar validación en cliente. No se muestra
			 * ningún mensaje de error. */

			respuesta.sendRedirect("menu");
			return;
		}

		/* Validación de longitudes de campos. */
		if (nombre.length() < 3 || nombre.length() > 25 ||
			apPaterno.length() < 3 || apPaterno.length() > 25 ||
			apMaterno.length() < 3 || apMaterno.length() > 25 ||
			CURP.length() != 18 ||
			direccion.length() < 10 || direccion.length() > 50 ||
			correo.length() < 10 || correo.length() > 50) {
			
			/* Validaciones hechas previamente en el cliente; si resultan
			 * falsas en este punto se trata de un usuario malicioso:
			 * no se muestra mensaje de error. */

			respuesta.sendRedirect("menu");
			return;
		}

		/* Validaciones de CURP y de email */

		/* Se guarda en la sesión los datos introducidos y se redirecciona
		 * de regreso al formulario. 
		 * TODO: Faltan estas trayectorias alternativas en CU1. */

		if (!validaciones.validarCURP(CURP)) {
			HttpSession sesion = peticion.getSession(true);
			sesion.setAttribute("error_curp", "");
			guardarDatosEnSesion(sesion);
			respuesta.sendRedirect("registrar");
			return;
		}

		if (!validaciones.validarCorreo(correo)) {
			HttpSession sesion = peticion.getSession(true);
			sesion.setAttribute("error_email", "");
			guardarDatosEnSesion(sesion);
			respuesta.sendRedirect("registrar");
			return;
		}

		/* Construir fecha de nacimiento */
		String[] partes = fecha2.split(" ");
		int dia = Integer.parseInt(partes[0]);
		int mes = diccionarioMeses.get(partes[1]);
		int anio = Integer.parseInt(partes[2]);
		fecha = new Date(anio, mes, dia);

		/* Insertar en base de datos */
		try {
			conexionBD.agregarRegistro(this);
		} catch (Exception e) {
			/* Curp duplicado
			 * TODO: contemplar otras posibles excepciones. */
			HttpSession sesion = peticion.getSession(true);
			sesion.setAttribute("curp_duplicado", true);
			guardarDatosEnSesion(sesion);
			respuesta.sendRedirect("registrar");
			return;
		}

		/* Mesaje de finalización */
		HttpSession sesion = peticion.getSession(true);
		sesion.setAttribute("registro_exitoso", true);
		respuesta.sendRedirect("registrar");
	}

	public void construirPaginaConsulta (HttpServletRequest peticion,
	HttpServletResponse respuesta)
	throws IOException, ServletException 
	{
		/* Validación de registros */
		boolean registros = false;
		try {
			registros = conexionBD.existenRegistros();
		} catch (Exception e) {
			respuesta.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
				"Excepción en construcción de página de consultas" + e);	
		}

		if (!registros) {
			/* Sin registros: Trayectoria alternativa A. */
			peticion.setAttribute("sin_registros", "");
		}

		/* Búsqueda de mensajes en sesión */
		HttpSession sesion = peticion.getSession(true);
		if(sesion.getAttribute("despues_de_consulta") != null) {
			peticion.setAttribute("despues_de_consulta", "");
			peticion.setAttribute("consulta", sesion.getAttribute("consulta"));
			peticion.setAttribute("parametro", sesion.getAttribute("parametro"));
			sesion.invalidate();
		}

		RequestDispatcher vista =
			peticion.getRequestDispatcher("/consultar.jsp");
		vista.forward(peticion, respuesta);
	}

	public void consultarDatos (HttpServletRequest peticion,
	HttpServletResponse respuesta)
	throws IOException, ServletException
	{
		String[] consulta = peticion.getParameter("curp").split(",");
		ArrayList<ArrayList<String>> registro = null;

		try {
			registro = conexionBD.realizarConsulta(consulta);
		} catch (Exception e) {
			e.printStackTrace();
			respuesta.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE,
					"Excepción SQL: " + e);
			return;
		}

		HttpSession sesion = peticion.getSession(true);
		sesion.setAttribute("despues_de_consulta", "");
		sesion.setAttribute("consulta", registro);
		sesion.setAttribute("parametro", peticion.getParameter("curp"));
		respuesta.sendRedirect("consultar");
	}

	private void guardarDatosEnSesion (HttpSession sesion)
	{
		sesion.setAttribute("nombre", nombre);
		sesion.setAttribute("apPaterno", apPaterno);
		sesion.setAttribute("apMaterno", apMaterno);
		sesion.setAttribute("curp", CURP);
		sesion.setAttribute("fecha", fecha2);
		sesion.setAttribute("sexo", sexo);
		sesion.setAttribute("direccion", direccion);
		sesion.setAttribute("correo", correo);
	}

	private void sesionAAtributos (HttpSession sesion, 
	HttpServletRequest peticion)
	{
		peticion.setAttribute("nombre", sesion.getAttribute("nombre"));
		peticion.setAttribute("apPaterno", sesion.getAttribute("apPaterno"));
		peticion.setAttribute("apMaterno", sesion.getAttribute("apMaterno"));
		peticion.setAttribute("curp", sesion.getAttribute("curp"));
		peticion.setAttribute("fecha", sesion.getAttribute("fecha"));
		peticion.setAttribute("sexo", sesion.getAttribute("sexo"));
		peticion.setAttribute("direccion", sesion.getAttribute("direccion"));
		peticion.setAttribute("correo", sesion.getAttribute("correo"));
	}
}
