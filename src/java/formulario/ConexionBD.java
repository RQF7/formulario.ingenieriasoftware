/* ConexionBD.java
 * Este archivo es parte de la aplicación web «Formulario».
 * Fecha de cración: lunes 18 de septiembre de 2017 */

package formulario;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Clase para gestionar las conexiones con la base de datos.
 * @author Ricardo Quezada
 */

public class ConexionBD {

	private Connection conexion = null;
    private Statement instruccion = null;
    private PreparedStatement preInstruccion = null;
    private ResultSet resultado = null;

	/** 
	 * Establecimiento de conexión con base de datos.
	 *
	 * Esta parte del código puede ser fuente de muchos errores si no se 
	 * configura de manera correcta:
	 *  -> Tener instalado y configurado mysql o mariaDB.
	 *  -> Cargar el script formulario.sql en una nueva base llamada 
	 *     «formulario»
	 *  -> Crear y dar permisos a un nuevo usuario (abajo se especifican 
	 *     los detalles)
	 *  -> Incluir en el classpath de la compilación el jar de JDBC.
	 *
	 * IMPORTANTE: 
	 * Usuario: «ingenieriasoftware»
	 * Contraseña: «123456»
	 * */

	public ConexionBD ()
	throws ClassNotFoundException, SQLException
	{
		Class.forName("com.mysql.jdbc.Driver");
		conexion = DriverManager
			.getConnection("jdbc:mysql://localhost:3306/formulario",
					"ingenieriasoftware", "123456");
	}

	/**
	 * Agrega un nuevo usuario a la base de datos según el contenido
	 * de los campos del formulario dado.
	 * Como el curp es llave primaria, si se ingresa uno ya existente,
	 * mysql lanza una excepción.
	 *
	 * TODO: Potencial fuente de errores: la clase Formulario hereda de 
	 * la clase Servlet, por lo que un solo objeto de estos sirve a varios
	 * (¿A todos?) los clientes. ¿Que pasa con los miembros de la clase
	 * si hay varias peticiones activas a la vez? Posiblemente haya que quitar
	 * los datos del cliente de la clase y hacer una nueva clase independiente
	 * (Cuestión de diseño).
	 * */

	public void agregarRegistro (Formulario formulario)
	throws SQLException
	{
		/* Obtener id de sexo */
		instruccion = conexion.createStatement();
		resultado = instruccion
			.executeQuery("select idSexo from Sexo where nombre = '" +
					formulario.sexo + "'");
		resultado.next();
		int idSexo = Integer.parseInt(String.valueOf(resultado.getObject(1)));

		preInstruccion = conexion.prepareStatement
				("insert into Usuario values (?, ?, ?, ?, ?, ?, ?, ?)");
		preInstruccion.setString(1, formulario.CURP);
		preInstruccion.setString(2, formulario.nombre);
		preInstruccion.setString(3, formulario.apPaterno);
		preInstruccion.setString(4, formulario.apMaterno);
		preInstruccion.setDate(5, new java.sql.Date(formulario.fecha.getTime()));
		preInstruccion.setInt(6, idSexo);
		preInstruccion.setString(7, formulario.direccion);
		preInstruccion.setString(8, formulario.correo);
		preInstruccion.executeUpdate();
	}

	/** 
	 * Regresa un ArrayList con el contenido de la tupla que coincida con
	 * el curp dado, si no hay ninguna coincidencia, se regrea null.
	 * */

	public ArrayList<ArrayList<String>> realizarConsulta (String[] curps)
	throws SQLException
	{
		ArrayList<ArrayList<String>> registros = 
			new ArrayList<ArrayList<String>>();

		for (String curp : curps) {

			/* Para quitar posibles espacios en blanco. */
			String[] filtro = curp.split(" ");
			for (int i=0; ; i++) {
				if (filtro[i].length() > 2) {
					curp = filtro[i];
					break;
				}
			}

			/* Realizar consulta. */
			preInstruccion = conexion
				.prepareStatement("select * from Usuario where CURP = ?");
			preInstruccion.setString(1, curp);
			resultado = preInstruccion.executeQuery();
			ArrayList<String> registro = new ArrayList<String>();

			if (resultado.next()) {
				registro.add(String.valueOf(resultado.getObject(1)));
				registro.add(String.valueOf(resultado.getObject(2)));
				registro.add(String.valueOf(resultado.getObject(3)));
				registro.add(String.valueOf(resultado.getObject(4)));
				registro.add(String.valueOf(resultado.getObject(5)));
				registro.add(String.valueOf(resultado.getObject(7)));
				registro.add(String.valueOf(resultado.getObject(8)));
				instruccion = conexion.createStatement();
				resultado = instruccion
					.executeQuery("select nombre from Sexo where idSexo = " +
							resultado.getObject(6));
				resultado.next();
				registro.add(String.valueOf(resultado.getObject(1)));
				registros.add(registro);
			} else {
				registro.add(curp);
				registros.add(registro);
			}
		}

		return registros;
	}

	/** 
	 * Método para obtener el catálogo del sexo.
	 * El contenido se almacena en un ArrayList.
	 * */

	public ArrayList<String> obtenerCatalogoSexo ()
	throws SQLException
	{
		ArrayList<String> catalogo = new ArrayList<String>();
		instruccion = conexion.createStatement();
		resultado = instruccion.executeQuery("select nombre from Sexo");
		while (resultado.next()) {
			catalogo.add(String.valueOf(resultado.getObject(1)));
		}
		return catalogo;
	}

	/** 
	 * Evalúa la existencia de al menos un registro en la base.
	 * */

	public boolean existenRegistros ()
	throws SQLException
	{
		instruccion = conexion.createStatement();
		resultado = instruccion.executeQuery("select nombre from Usuario");
		if (resultado.next()) {
			return true;
		} else {
			return false;
		}
	}

}
